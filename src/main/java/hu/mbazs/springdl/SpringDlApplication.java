package hu.mbazs.springdl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDlApplication.class, args);
	}

}
