package hu.mbazs.springdl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class AppConfig extends WebSecurityConfigurerAdapter  {

	@Bean
	public RedisConnectionFactory redisConnectionFactory() {
		return new LettuceConnectionFactory(new RedisStandaloneConfiguration("localhost", 6379));
	}
	
	@Bean
	public RedisConnection redisConnection() {
		return redisConnectionFactory().getConnection();
	}

	@Override
	protected void configure(HttpSecurity http)
		throws Exception {
		http
			.sessionManagement(sm
				-> sm.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
			.authorizeRequests(ar
				-> ar.antMatchers("/",
						  "/hello")
				.permitAll());
	}
}
