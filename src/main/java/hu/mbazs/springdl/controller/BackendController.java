package hu.mbazs.springdl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class BackendController {

	private static final byte[] COUNTER_KEY = "ctr".getBytes();

	@Autowired
	private RedisConnection conn;

	@RequestMapping(value="/reset")
	public void resetCounter() {
		conn.getSet(COUNTER_KEY, "0".getBytes());
	}

	@RequestMapping(value="/get", produces = "text/plain")
	public String getAndIncrCounter() {
		String hello = Long.toString(conn.incr(COUNTER_KEY));
		return hello;
	}

	@RequestMapping(value="/json", produces = "application/json")
	public Object json() {
		HashMap<String, Object> hm = new HashMap<>();
		hm.put("elso", 1);
		hm.put("masodik", 2);
		hm.put("arr", new int[] { 1, 2, 3 } );
		return hm;
	}
}
